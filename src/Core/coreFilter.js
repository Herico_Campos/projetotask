const GetDH = () => {
  info = new Date();

  let dat = info.getMonth();
  let tm = dat + 1;

  const dataInfo =
    "Alterado em " +
    info.getDate() +
    "/" +
    tm +
    "/" +
    info.getFullYear() +
    " as " +
    info.getHours() +
    ":" +
    info.getMinutes() +
    ":" +
    info.getSeconds();

  return dataInfo;
};

module.exports = {
  GetDH, //Trata a data e o horario da alteraçao
};
