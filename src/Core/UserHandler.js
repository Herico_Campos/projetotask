const userModel = require("../Persistence/models/userModel");
const filter = require("./coreFilter");

class UserHandler {
  constructor() {}

  async gravar(req) {
    try {
      const { email, password, user, cpf } = req.body;
      const params_1 = [cpf];
      const userQuery = "SELECT * FROM cadastro_user WHERE cpf_db=?";
      const userInfo = await userModel(userQuery, params_1);

      if (userInfo.length > 0) throw new Error("usuario existente");

      const params_2 = [email, password, user, cpf];
      const query = `INSERT INTO cadastro_user(email_db,password_db,user_db,cpf_db)VALUES (?,?,?,?)`;

      await userModel(query, params_2);
      return { mensagem: "Usuario criada com sucesso!!!" };
    } catch (error) {
      return { message: error.message, code: 500 };
    }
  }

  async buscar() {
    try {
      const query = "SELECT * FROM cadastro_user";
      return await userModel(query);
    } catch (error) {
      return {
        message: "falha ao buscar usuarios no banco de dados",
        code: 500,
      };
    }
  }

  async deletar(req) {
    try {
      const { cpf } = req.body;
      const params_1 = [cpf];
      const userQuery = "SELECT * FROM cadastro_user WHERE cpf_db=?";
      const userInfo = await userModel(userQuery, params_1);

      if (userInfo.length == 0) throw new Error("Usuario nao existe!!");

      const query = "DELETE FROM cadastro_user WHERE cpf_db =?";

      await userModel(query, params_1);
      return { mensagem: "Usuario deletado com sucesso!!!" };
    } catch (error) {
      return { message: error.message, code: 500 };
    }
  }

  async atualizar(req) {
    try {
      const { id } = req.params;
      const { email, password, user, cpf } = req.body;
      const params_1 = [id];
      const userQuery = `SELECT * FROM cadastro_user WHERE id_db =?`;
      const userInfo = await userModel(userQuery, params_1);
      if (userInfo.length == 0) throw new Error("Usuario nao existe!!");

      const params_2 = [email, password, user, cpf, id];
      const query = `UPDATE cadastro_user SET email_db =?,password_db=?,user_db=?,cpf_db=? WHERE id_db =?`;
      await userModel(query, params_2);
      return { mensagem: "Usuario atualizado com sucesso!!!" };
    } catch (error) {
      return { message: error.message, code: 500 };
    }
  }

  async baixa(req) {
    try {
      const { cpf, estado } = req.body;
      const temp = filter.GetDH();
      const params_1 = [cpf];
      const userQuery = `SELECT * FROM cadastro_user WHERE cpf_db =?`;
      const userInfo = await userModel(userQuery, params_1);
      if (userInfo.length == 0) throw new Error("Usuario nao existe!!");

      const params_2 = [estado, temp, cpf];
      const query = `UPDATE cadastro_user SET status_db =?,info_status=? WHERE cpf_db =?`;
      await userModel(query, params_2);
      return { mensagem: "Baixa no usuario com sucesso!!!" };
    } catch (error) {
      return { message: error.message, code: 500 };
    }
  }

  async buscarQuery(req) {
    try {
      const info = req.query.info;
      const query = "SELECT * FROM cadastro_user WHERE status_db =?";
      const userInfo = await userModel(query, info);
      const infolength = userInfo.length;
      console.log(infolength + " KKKk");
      if (infolength > 0) return userInfo;
      return { message: "nenhuma baixa encontrada!!!" };
    } catch (error) {
      return {
        message: "falha ao buscar usuarios no banco de dados",
        code: 500,
      };
    }
  }

  async buscarCpf(req) {
    try {
      const { cpf } = req.body;
      const params_1 = [cpf];
      const userQuery = "SELECT * FROM cadastro_user WHERE cpf_db=?";
      const userInfo = await userModel(userQuery, params_1);

      if (userInfo.length == 0) throw new Error("Usuario nao existe!!");
      return userInfo;
    } catch (error) {
      return { message: error.message, code: 500 };
    }
  }
}

module.exports = UserHandler;
