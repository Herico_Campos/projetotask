const express = require("express");
const router = express.Router();
const UsersHandler = require("../../Core/UserHandler");
const handler = new UsersHandler();
const midlleware = require("../../middleware/middlewares");

router.get("/listar", async (req, res) => {
  try {
    const user = await handler.buscar();
    if (user) return res.status(200).send(user);
  } catch (error) {
    console.log(JSON.stringify(error));
    return res.status(404).json(JSON.stringify(error));
  }
});

router.post("/gravar", midlleware.Valid_cpf, async (req, res) => {
  try {
    const user = await handler.gravar(req);
    if (user) return res.status(200).send(user);
  } catch (error) {
    console.log(JSON.stringify(error));
    return res.status(404).json(JSON.stringify(error));
  }
});

router.delete("/deletar", midlleware.Valid_cpf, async (req, res) => {
  try {
    const user = await handler.deletar(req);
    if (user) return res.status(200).send(user);
  } catch (error) {
    console.log(JSON.stringify(error));
    return res.status(404).json(JSON.stringify(error));
  }
});

router.put(
  "/atualizar/:id",
  midlleware.validateId,
  midlleware.Valid_cpf,
  async (req, res) => {
    try {
      const user = await handler.atualizar(req);
      if (user) return res.status(200).send(user);
    } catch (error) {
      console.log(JSON.stringify(error));
      return res.status(404).json(JSON.stringify(error));
    }
  }
);

router.put("/baixa", midlleware.Valid_cpf, async (req, res) => {
  try {
    const user = await handler.baixa(req);
    if (user) return res.status(200).send(user);
  } catch (error) {
    console.log(JSON.stringify(error));
    return res.status(404).json(JSON.stringify(error));
  }
});

router.get("/listarQuery", async (req, res) => {
  try {
    const user = await handler.buscarQuery(req);
    if (user) return res.status(200).send(user);
  } catch (error) {
    console.log(JSON.stringify(error));
    return res.status(404).json(JSON.stringify(error));
  }
});

router.get("/listarCpf", midlleware.Valid_cpf, async (req, res) => {
  try {
    const user = await handler.buscarCpf(req);
    if (user) return res.status(200).send(user);
  } catch (error) {
    console.log(JSON.stringify(error));
    return res.status(404).json(JSON.stringify(error));
  }
});

module.exports = router;
