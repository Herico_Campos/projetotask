require("dotenv").config();
const express = require("express");
const app = express();
const TaskController = require("../src/Web/controllers/tasksController");
const UserController = require("../src/Web/controllers/userController");
const Cors = require("cors");

app.use(express.json());
app.use(Cors());
app.use("/api/tasks/", TaskController);
app.use("/api/user/", UserController);

module.exports = app;

const port = process.env.porta || 3030;

app.listen(port, () => console.log("servidor rodando na porta " + port));
